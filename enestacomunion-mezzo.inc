\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key a \major

		R1*2  |
		e' 2 ( cis'  |
		b 2 ) fis' 4 gis'  |
%% 5
		e' 8 fis' e' 4 ( cis' 2  |
		b 2. ) r4  |
		e' 2 ( cis'  |
		b 2 ) cis' 4 b  |
		cis' 8 d' cis' ( d' cis' d' fis' 4  |
%% 10
		eis' 2. ) r4  |
		cis' 2 e'  |
		fis' 4. fis' 8 gis' fis' e' cis'  |
		d' 8 e' cis' 4 ( dis' 2  |
		cis' 2 dis' 4. ) r8  |
%% 15
		cis' 2 e'  |
		fis' 2 gis' 8 fis' e' cis'  |
		d' 2 ( ~ d'  |
		b 2 ~ b 4. ) a 8  |
		b 1 ~  |
%% 20
		b 2 b 8. a g 8  |
		c' 2 ( bes  |
		a 2 ) d' 8. ( ees' d' 8 )  |
		bes 2 ( a  |
		gis 2 ) r  |
%% 25
		R1  |
		r2 g' 8. g' g' 8  |
		fis' 2 d' 4 ( e'  |
		f' 4. ) r8 f' 8. f' f' 8  |
		e' 2 cis' 4 ( d'  |
%% 30
		dis' 4. ) r8 dis' 8. dis' dis' 8  |
		d' 2 b 4 ( c'  |
		d' 8 ) r b cis' d' e' fis' gis'  |
		a' 2. ( gis' 4  |
		g' 4 ) r g' 8. g' g' 8  |
%% 35
		fis' 2 d' 4 ( e'  |
		f' 4. ) r8 f' 8. f' f' 8  |
		e' 2 cis' 4 ( d'  |
		dis' 4. ) r8 dis' 8. dis' dis' 8  |
		d' 2 b 4 ( c'  |
%% 40
		d' 4. ) r8 d' 2  |
		e' 1 (  |
		fis' 1  |
		e' 2. ) r4  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		%
		En __ es -- ta co -- mu -- nión __
		ve -- ni mos a pe -- dir __
		que va -- ya -- mos to -- dos jun -- tos a vi -- vir __
		al pa -- ra -- í -- so del a -- mor, __
		a -- mor, __
		don -- de "tú es" -- tás, __
		a -- hí, __
%		a -- quí. __

		Pa -- "ra a" -- la -- bar -- te, __
		pa -- "ra a" -- do -- rar -- te, __
		sen -- tir -- nos jun -- tos, __
		y te -- ner tu ben -- di -- ción. __

		Pa -- "ra a" -- la -- bar -- te, __
		pa -- "ra a" -- do -- rar -- te, __
		sen -- tir -- me jun -- to, __
		de ti. __
	}
>>
