\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key a \major

		R1*2  |
		e' 1 ~  |
		e' 2 fis' 4 gis'  |
%% 5
		e' 8 fis' e' 2. ~  |
		e' 2. r4  |
		e' 1 ~  |
		e' 2 cis' 4 b  |
		cis' 8 d' cis' ( d' cis' d' cis' d'  |
%% 10
		cis' 2. ) r4  |
		fis' 2 gis'  |
		a' 4. a' 8 b' a' gis' e'  |
		fis' 8 gis' fis' 4 ~ fis' 2 ~  |
		fis' 2 ~ fis' 4. r8  |
%% 15
		fis' 2 gis'  |
		a' 2 b' 8 a' gis' e'  |
		g' 2 ( fis'  |
		e' 2 d' 4. ) d' 8  |
		e' 1 ~  |
%% 20
		e' 2 e' 8. d' c' 8  |
		ees' 1 ~  |
		ees' 2 f' 8. ( g' f' 8 )  |
		d' 1 ~  |
		d' 2 e' 8. ( fis' e' 8 )  |
%% 25
		cis' 1 ~  |
		cis' 4 r cis'' 8. b' a' 8  |
		cis'' 2 b' ~  |
		b' 4. r8 b' 8. a' gis' 8  |
		b' 2 a' ~  |
%% 30
		a' 4. r8 a' 8. gis' fis' 8  |
		a' 2 gis' ~  |
		gis' 8 r e' fis' gis' gis' a' b'  |
		cis'' 1 ~  |
		cis'' 4 r cis'' 8. b' a' 8  |
%% 35
		cis'' 2 b' ~  |
		b' 4. r8 b' 8. a' gis' 8  |
		b' 2 a' ~  |
		a' 4. r8 a' 8. gis' fis' 8  |
		a' 2 gis' ~  |
%% 40
		gis' 4. r8 b' 2  |
		a' 1 ~  |
		a' 1 ~  |
		a' 2. r4  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		%
		En __ es -- ta co -- mu -- nión __
		ve -- ni mos a pe -- dir __
		que va -- ya -- mos to -- dos jun -- tos a vi -- vir __
		al pa -- ra -- í -- so del a -- mor, __
		a -- mor, __
		don -- de "tú es" -- tás, __
		a -- hí, __
		a -- quí. __

		Pa -- "ra a" -- la -- bar -- te, __
		pa -- "ra a" -- do -- rar -- te, __
		sen -- tir -- nos jun -- tos, __
		y te -- ner tu ben -- di -- ción. __

		Pa -- "ra a" -- la -- bar -- te, __
		pa -- "ra a" -- do -- rar -- te, __
		sen -- tir -- me jun -- to, __
		de ti. __		
	}
>>
