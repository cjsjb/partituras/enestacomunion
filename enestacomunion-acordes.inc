\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t
		% intro
		a1:maj7 d1:maj7

		% en esta comunion....
		a1:maj7 d1:maj7
		a1:maj7 d1:maj7
		a1:maj7 d1:maj7
		cis1:7sus4 cis1:7

		% que vayamos...
		fis2:m e2
		fis2:m e2
		fis2:m b2
		fis2:m b2
		% al paraiso...
		fis2:m e2
		fis2:m e2

		% amor...
		g1 g1
		c1:maj7 c1:maj7
		c1:m7 f1:7
		bes1:maj7 e1:7
		a1:maj7

		% para alabarte...
		e2:m a2:7 d1:maj7
		f1/g cis1:m
		a1/fis b1:m7
		e1:7 a1:maj7
		% para alabarte...
		e2:m a2:7 d1:maj7
		f1/g cis1:m
		a1/fis b1:m7
		e1:7 a1:maj7

		d1:maj7 a1:maj7
	}
